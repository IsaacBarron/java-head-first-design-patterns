package com.isaaccode.pattern.adapter;

public class AdapterPatternMain
{
    public static void main(String[] args)
    {
        /*
        This will contain the logic
         */
        TartgetAdaptee apaptee = new TartgetAdaptee();

        // This will solve the compatibility problem.
        TargetAdapter targetInterface = new TargetAdapter(apaptee);

        // The client will call the interface
        Client client = new Client(targetInterface);
    }
}
