package com.isaaccode.pattern.adapter;

public class Client
{
    private TargetInterface targetInterface;

    public Client(TargetInterface targetInterface)
    {
        this.targetInterface = targetInterface;
    }

    public void callInterfaceAdapter()
    {
        targetInterface.request();
    }
}
