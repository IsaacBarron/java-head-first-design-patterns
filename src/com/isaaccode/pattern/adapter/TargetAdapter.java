package com.isaaccode.pattern.adapter;

public class TargetAdapter implements TargetInterface
{
    private TartgetAdaptee adaptee;

    public TargetAdapter(TartgetAdaptee apaptee)
    {
        this.adaptee = apaptee;
    }

    @Override
    public void request()
    {
        adaptee.doSomething();
    }
}
