package com.isaaccode.pattern.adapter;

public interface TargetInterface
{
    void request();
}
