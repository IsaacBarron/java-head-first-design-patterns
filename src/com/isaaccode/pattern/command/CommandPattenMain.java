package com.isaaccode.pattern.command;

import com.isaaccode.pattern.command.command.DownCommand;
import com.isaaccode.pattern.command.command.LeftCommand;
import com.isaaccode.pattern.command.command.RightCommand;
import com.isaaccode.pattern.command.command.UpCommand;
import com.isaaccode.pattern.command.invoker.RemoteController;
import com.isaaccode.pattern.command.receiver.Bot;

public class CommandPattenMain
{
    public static void main(String[] args)
    {
        /*
        The bot is the receiver. it will contain the actions
         */
        Bot bot = new Bot();

        /*
        The remote controller will be passed commands to and the commands will invoke the receiver
         */
        RemoteController remoteController =
                new RemoteController(
                        new UpCommand(bot),
                        new DownCommand(bot),
                        new LeftCommand(bot),
                        new RightCommand(bot)
                );

        /*
        Just checking to see if the controller works correctly
         */
        remoteController.downButton();
        remoteController.upButton();
        remoteController.leftButton();
        remoteController.rightButton();
        remoteController.downButton();

        System.out.println("Testing undo all");
        System.out.println();
        remoteController.undoAll();

        System.out.println("Testing undo");
        System.out.println();

        remoteController.leftButton();
        remoteController.undo();
    }
}
