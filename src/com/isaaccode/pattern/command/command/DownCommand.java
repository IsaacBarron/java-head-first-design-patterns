package com.isaaccode.pattern.command.command;

import com.isaaccode.pattern.command.receiver.Bot;

public class DownCommand implements Commend
{
    private Bot bot;

    public DownCommand(Bot bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        bot.moveDown();
    }

    @Override
    public void unexecute()
    {
        bot.moveUp();
    }
}
