package com.isaaccode.pattern.command.command;

import com.isaaccode.pattern.command.receiver.Bot;

public class LeftCommand implements Commend
{
    private Bot bot;

    public LeftCommand(Bot bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        bot.moveLeft();
    }

    @Override
    public void unexecute()
    {
        bot.moveRight();
    }
}
