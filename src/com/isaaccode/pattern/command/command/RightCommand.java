package com.isaaccode.pattern.command.command;

import com.isaaccode.pattern.command.receiver.Bot;

public class RightCommand implements Commend
{
    private Bot bot;

    public RightCommand(Bot bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        bot.moveRight();
    }

    @Override
    public void unexecute()
    {
        bot.moveLeft();
    }
}
