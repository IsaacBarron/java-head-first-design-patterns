package com.isaaccode.pattern.command.command;

import com.isaaccode.pattern.command.receiver.Bot;

public class UpCommand implements Commend
{
    private Bot bot;

    public UpCommand(Bot bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        bot.moveUp();
    }

    @Override
    public void unexecute()
    {
        bot.moveDown();
    }
}