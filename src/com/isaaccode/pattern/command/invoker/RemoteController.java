package com.isaaccode.pattern.command.invoker;

import com.isaaccode.pattern.command.command.Commend;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class RemoteController
{
    private Commend upCommand;
    private Commend downCommand;
    private Commend leftCommand;
    private Commend rightCommand;

    private  Queue<Commend> commandQueue = new LinkedBlockingQueue<>();


    private RemoteController() {}

    public RemoteController(Commend upCommand, Commend downCommand, Commend leftCommand, Commend rightCommand)
    {
        this.upCommand = upCommand;
        this.downCommand = downCommand;
        this.leftCommand = leftCommand;
        this.rightCommand = rightCommand;
    }

    public void upButton()
    {
        upCommand.execute();
        commandQueue.add(upCommand);
    }

    public void downButton()
    {
        downCommand.execute();
        commandQueue.add(downCommand);
    }

    public void rightButton()
    {
        rightCommand.execute();
        commandQueue.add(rightCommand);
    }

    public void leftButton()
    {
        leftCommand.execute();
        commandQueue.add(leftCommand);
    }

    public void undo()
    {
        Commend commend = commandQueue.poll();
        if(commend != null)
        {
            commend.unexecute();
        }
    }

    public void undoAll()
    {
        while (!commandQueue.isEmpty())
        {
            commandQueue.poll().unexecute();
        }
    }
}
