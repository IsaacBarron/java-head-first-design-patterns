package com.isaaccode.pattern.decorator;

import com.isaaccode.pattern.decorator.condiments.Mocha;
import com.isaaccode.pattern.decorator.drink.Beverage;
import com.isaaccode.pattern.decorator.drink.HouseBlend;

public class DecoratorPatternMain
{
    public static void main(String[] args)
    {
        Beverage houseBlend = new HouseBlend();

        houseBlend = new Mocha(houseBlend);

        System.out.println(houseBlend.getDescription());
        System.out.println(houseBlend.cost());
    }
}
