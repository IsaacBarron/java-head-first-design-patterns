package com.isaaccode.pattern.decorator.condiments;

import com.isaaccode.pattern.decorator.drink.Beverage;

public abstract class CondimentDecorator extends Beverage
{
    public abstract String getDescription();
}
