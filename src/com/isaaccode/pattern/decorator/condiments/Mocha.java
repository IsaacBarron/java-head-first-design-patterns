package com.isaaccode.pattern.decorator.condiments;

import com.isaaccode.pattern.decorator.drink.Beverage;

public class Mocha extends CondimentDecorator
{
    Beverage beverage;

    public Mocha(Beverage beverage)
    {
        this.beverage = beverage;
    }

    @Override
    public String getDescription()
    {
        return beverage.getDescription() + ", Mocha";
    }

    @Override
    public double cost()
    {
        return beverage.cost() + .20;
    }
}
