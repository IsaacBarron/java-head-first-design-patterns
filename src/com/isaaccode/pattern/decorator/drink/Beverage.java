package com.isaaccode.pattern.decorator.drink;

public abstract class Beverage
{
    String description = "Unknown Beverage";

    public String getDescription()
    {
        return description;
    }

    public abstract double cost();
}
