package com.isaaccode.pattern.factory;

import com.isaaccode.pattern.factory.store.PizzaStore;
import com.isaaccode.pattern.factory.store.region.dallas.DallasPizzaStore;

public class FactoryPatternMain
{
    public static void main(String[] args)
    {
        PizzaStore pizzaStore = new DallasPizzaStore();

        pizzaStore.orderPizza("Cheese");
    }
}
