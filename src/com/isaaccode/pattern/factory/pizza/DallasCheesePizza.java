package com.isaaccode.pattern.factory.pizza;

public class DallasCheesePizza extends Pizza
{
    private String name = "Dallas Cheese Pizza";

    @Override
    public String prepare()
    {
        return "Preparing " + getName();
    }

    @Override
    public double cost()
    {
        return 10.00;
    }

    @Override
    public String getName()
    {
        return name;
    }
}
