package com.isaaccode.pattern.factory.pizza;

public abstract class Pizza
{
    private String name;
    private String sauce;
    private double cost;

    private String description = "";

    public abstract String prepare();

    public abstract double cost();

    public void bake()
    {
        System.out.println("Bake for 25 minutes at 350");
    }

    public void cut()
    {
        System.out.println("Cut into 8 slices");
    }

    public void box()
    {
        System.out.println("Box it up");
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }
}
