package com.isaaccode.pattern.factory.pizza.dough;

import com.isaaccode.pattern.factory.pizza.Pizza;

public abstract class Dough extends Pizza
{
    public abstract String getDescription();
}
