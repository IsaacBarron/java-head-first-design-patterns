package com.isaaccode.pattern.factory.pizza.dough;

import com.isaaccode.pattern.factory.pizza.Pizza;

public class ThickOilyCrust extends Dough
{
    private static final String NAME = "Think Oily Crust";

    private Pizza pizza;

    public ThickOilyCrust(Pizza pizza)
    {
        this.pizza = pizza;
    }

    @Override
    public String prepare()
    {
        return pizza.prepare() + ",\nUsing " + NAME;
    }

    @Override
    public String getDescription()
    {
        return prepare();
    }

    @Override
    public double cost()
    {
        return pizza.cost() + 1.00;
    }
}
