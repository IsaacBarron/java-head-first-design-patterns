package com.isaaccode.pattern.factory.pizza.toppings;

import com.isaaccode.pattern.factory.pizza.Pizza;

public class Hamburger extends ToppingsDecorator
{
    private Pizza pizza;
    private static final String name = "Hamburger";

    public Hamburger(Pizza pizza)
    {
        this.pizza = pizza;
    }

    @Override
    public String getDescription()
    {
        return prepare();
    }

    @Override
    public String prepare()
    {
        return pizza.prepare() + "\n\t applying topping " + name;
    }

    @Override
    public double cost()
    {
        return pizza.cost() + 1.34;
    }
}
