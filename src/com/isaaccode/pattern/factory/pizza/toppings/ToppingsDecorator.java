package com.isaaccode.pattern.factory.pizza.toppings;

import com.isaaccode.pattern.factory.pizza.Pizza;

public abstract class ToppingsDecorator extends Pizza
{
    public abstract String getDescription();
}
