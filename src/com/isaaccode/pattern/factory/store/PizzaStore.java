package com.isaaccode.pattern.factory.store;

import com.isaaccode.pattern.factory.pizza.Pizza;

public abstract class PizzaStore
{
    public Pizza orderPizza(String type)
    {
        Pizza pizza = createPizza(type);

        pizza.prepare();
        System.out.println(pizza.getDescription());
        pizza.bake();
        pizza.cut();
        pizza.box();
        System.out.println("Cost:\t" + pizza.cost());

        return pizza;
    }

    protected abstract Pizza createPizza(String type);
}
