package com.isaaccode.pattern.factory.store.region.dallas;

import com.isaaccode.pattern.factory.pizza.DallasCheesePizza;
import com.isaaccode.pattern.factory.pizza.Pizza;
import com.isaaccode.pattern.factory.pizza.dough.ThickOilyCrust;
import com.isaaccode.pattern.factory.pizza.toppings.Hamburger;
import com.isaaccode.pattern.factory.store.PizzaStore;

public class DallasPizzaStore extends PizzaStore
{
    @Override
    protected Pizza createPizza(String type)
    {
        switch (type)
        {
            case "Cheese":
                return new Hamburger(new ThickOilyCrust(new DallasCheesePizza()));
            default:
                return null;
        }
    }
}
