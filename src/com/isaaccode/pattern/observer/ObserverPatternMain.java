package com.isaaccode.pattern.observer;

import com.isaaccode.pattern.observer.observer.CurrentConditionsObserver;
import com.isaaccode.pattern.observer.observer.CurrentConditionsObserver2;
import com.isaaccode.pattern.observer.subject.WeatherData;

public class ObserverPatternMain
{
    public static void main(String[] args)
    {
        WeatherData weatherData = new WeatherData();
        CurrentConditionsObserver currentConditionsObserver = new CurrentConditionsObserver("CurrentConditionsObserver", weatherData);
        CurrentConditionsObserver2 currentConditionsObserver2 = new CurrentConditionsObserver2("CurrentConditionsObserver2", weatherData);


        weatherData.setMeasurements(105, 31, 10);
        weatherData.setMeasurements(2, 100, 40);
        weatherData.setMeasurements(105, 31, 10);

        weatherData.removerObserver(currentConditionsObserver);

        weatherData.setMeasurements(2, 100, 40000);
    }
}
