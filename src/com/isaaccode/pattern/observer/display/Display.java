package com.isaaccode.pattern.observer.display;

public interface Display
{
    public void display();
}
