package com.isaaccode.pattern.observer.observer;

import com.isaaccode.pattern.observer.display.Display;
import com.isaaccode.pattern.observer.subject.Subject;

public class CurrentConditionsObserver implements Observer, Display
{
    private float temp;
    private float humidity;
    private float pressure;
    private String name;

    private Subject subject;

    public CurrentConditionsObserver(String name, Subject subject)
    {
        this.subject = subject;
        this.name = name;
        subject.registerObserver(this);
    }

    @Override
    public void display()
    {
        System.out.println("****************************************************  " + this.name.toUpperCase() + "   ****************************************************");
        System.out.println("Current conditions: " + this.temp + "F degrees, " +
                "Pressure is " + this.pressure +  " and " + this.humidity + "% humidity");
        System.out.println("***************************************************************************************************************************************");
    }

    @Override
    public void update(float temp, float pressure, float humidity)
    {
        this.humidity = humidity;
        this.temp = temp;
        this.pressure = pressure;
        this.display();
    }
}
