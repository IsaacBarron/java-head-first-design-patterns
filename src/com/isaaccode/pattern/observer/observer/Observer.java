package com.isaaccode.pattern.observer.observer;

public interface Observer
{
    void update(float temp, float pressure, float humidity);
}
