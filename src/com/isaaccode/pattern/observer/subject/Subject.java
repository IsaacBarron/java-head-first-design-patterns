package com.isaaccode.pattern.observer.subject;


import com.isaaccode.pattern.observer.observer.Observer;

public interface Subject
{
    public void registerObserver(Observer o);
    public void removerObserver(Observer o);
    public void notifyObservers();
}
