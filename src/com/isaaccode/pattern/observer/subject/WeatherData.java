package com.isaaccode.pattern.observer.subject;


import com.isaaccode.pattern.observer.observer.Observer;

import java.util.ArrayList;
import java.util.List;

public class WeatherData implements Subject
{
    private List<Observer> observers = new ArrayList<>();
    private float temp;
    private float humidity;
    private float pressure;

    @Override
    public void registerObserver(Observer o)
    {
        this.observers.add(o);
        System.out.println("added Observer:::".toUpperCase() + o.getClass().getName());
    }

    @Override
    public void removerObserver(Observer o)
    {
        int found = this.observers.indexOf(o);
        if (found >= 0)
        {
            System.out.println("Removing observer::".toUpperCase() + o.getClass().getName());
            this.observers.remove(found);
        }
    }

    @Override
    public void notifyObservers()
    {
        for (Observer o: observers)
        {
            o.update(this.temp, this.pressure, this.humidity);
        }
    }

    private void measurementsChanged()
    {
        this.notifyObservers();
    }

    public void setMeasurements(float temp, float humidity, float pressure)
    {
        this.temp = temp;
        this.humidity = humidity;
        this.pressure = pressure;
        this.measurementsChanged();
    }
}
