package com.isaaccode.pattern.singleton;

import com.isaaccode.pattern.singleton.singletons.NormalSingleton;

/**
 * Need to learn more about multithreading before leaving the pattern.
 */
public class SingletonPattenMain
{
    public static void main(String[] args)
    {
        /*
        The first getInstance call should create and assign id
        the second should not. Then the id calls should be the same.
         */
        NormalSingleton normalSingleton = NormalSingleton.getInstance();
        NormalSingleton normalSingleton2 = NormalSingleton.getInstance();


        System.out.println(normalSingleton.getId());
        System.out.println(normalSingleton2.getId());
    }
}
