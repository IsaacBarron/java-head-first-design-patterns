package com.isaaccode.pattern.singleton.singletons;

public class NormalSingleton
{
    private static NormalSingleton singleton;
    private double id;

    private NormalSingleton(){}

    public static NormalSingleton getInstance()
    {
        if(singleton == null)
        {
            singleton = new NormalSingleton();
            singleton.id = (int)(Math.random() * 50 + 1);
            System.out.println("Assigning Id:" + singleton.id);
        }
        return singleton;
    }

    public double getId()
    {
        return id;
    }
}
