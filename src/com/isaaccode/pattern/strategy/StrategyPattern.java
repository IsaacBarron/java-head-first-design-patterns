package com.isaaccode.pattern.strategy;

import com.isaaccode.pattern.strategy.ducks.CrazyDuck;
import com.isaaccode.pattern.strategy.ducks.Duck;
import com.isaaccode.pattern.strategy.ducks.NormalDuck;
import com.isaaccode.pattern.strategy.ducks.behavior.flying.RocketFuelFly;

/**\
 * This pattern allows you to pull out the elements that change allowing reuse of inheritance class
 * and to let you change behavior at run time.
 */
public class StrategyPattern
{
    public static void main(String[] args)
    {
        // Normal duck behavior
        System.out.println("Just a Normal Duck here".toUpperCase());
        NormalDuck normalDuck = new NormalDuck();
        normalDuck.performFly();
        normalDuck.performQuack();

        // Not Normal duck behavior and changing at run time
        System.out.println();
        System.out.println("Not Normal duck behavior and changing at run time".toUpperCase());
        normalDuck.setFlyingBehavior(new RocketFuelFly());

        // Muted Duck
        System.out.println();
        System.out.println("Crazy Duck!!".toUpperCase());
        CrazyDuck crazyDuck = new CrazyDuck();
        crazyDuck.performFly();
        crazyDuck.performQuack();

    }
}
