package com.isaaccode.pattern.strategy.ducks;

import com.isaaccode.pattern.strategy.ducks.behavior.flying.FlyingBehavior;
import com.isaaccode.pattern.strategy.ducks.behavior.flying.RocketFuelFly;
import com.isaaccode.pattern.strategy.ducks.behavior.quacking.QuakingBehavior;
import com.isaaccode.pattern.strategy.ducks.behavior.quacking.ScreamQuack;

public class CrazyDuck extends Duck
{
    public CrazyDuck()
    {
        super(new RocketFuelFly(), new ScreamQuack());
    }
}
