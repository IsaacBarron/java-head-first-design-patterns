package com.isaaccode.pattern.strategy.ducks;

import com.isaaccode.pattern.strategy.ducks.behavior.flying.FlyingBehavior;
import com.isaaccode.pattern.strategy.ducks.behavior.quacking.QuakingBehavior;

public abstract class Duck
{
    private FlyingBehavior flyingBehavior;
    private QuakingBehavior quakingBehavior;

    public Duck(FlyingBehavior flyingBehavior, QuakingBehavior quakingBehavior)
    {
        this.flyingBehavior = flyingBehavior;
        this.quakingBehavior = quakingBehavior;
    }

    public Duck() {}

    public void performQuack()
    {
        quakingBehavior.quack();
    }

    public  void performFly()
    {
        flyingBehavior.fly();
    }

    public void setFlyingBehavior(FlyingBehavior flyingBehavior)
    {
        this.flyingBehavior = flyingBehavior;
    }

    public void setQuakingBehavior(QuakingBehavior quakingBehavior)
    {
        this.quakingBehavior = quakingBehavior;
    }
}
