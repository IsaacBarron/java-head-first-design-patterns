package com.isaaccode.pattern.strategy.ducks;

import com.isaaccode.pattern.strategy.ducks.behavior.flying.NormalFly;
import com.isaaccode.pattern.strategy.ducks.behavior.quacking.NormalQuack;

public class NormalDuck extends Duck
{
    public NormalDuck()
    {
        super(new NormalFly(), new NormalQuack());
    }
}
