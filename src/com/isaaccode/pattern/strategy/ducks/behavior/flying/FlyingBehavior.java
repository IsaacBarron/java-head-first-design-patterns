package com.isaaccode.pattern.strategy.ducks.behavior.flying;

public interface FlyingBehavior
{
    void fly();
}
