package com.isaaccode.pattern.strategy.ducks.behavior.flying;

public class NormalFly implements FlyingBehavior
{
    @Override
    public void fly()
    {
        System.out.println("Normal Flight!!!".toUpperCase());
    }
}
