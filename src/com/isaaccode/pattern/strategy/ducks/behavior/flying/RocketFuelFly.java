package com.isaaccode.pattern.strategy.ducks.behavior.flying;

public class RocketFuelFly implements FlyingBehavior
{
    @Override
    public void fly()
    {
        System.out.println("Rocket Fly!!!".toUpperCase());
    }
}
