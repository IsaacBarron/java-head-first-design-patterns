package com.isaaccode.pattern.strategy.ducks.behavior.quacking;

public class NormalQuack implements QuakingBehavior
{
    @Override
    public void quack()
    {
        System.out.println("Normal Quack!!!".toUpperCase());
    }
}
