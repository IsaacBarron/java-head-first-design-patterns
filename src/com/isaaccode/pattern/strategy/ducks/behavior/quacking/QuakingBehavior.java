package com.isaaccode.pattern.strategy.ducks.behavior.quacking;

public interface QuakingBehavior
{
    void quack();
}
