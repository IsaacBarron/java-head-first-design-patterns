package com.isaaccode.pattern.strategy.ducks.behavior.quacking;

public class ScreamQuack implements QuakingBehavior
{
    @Override
    public void quack()
    {
        System.out.println("Screaming Duck Quack".toUpperCase());
    }
}
